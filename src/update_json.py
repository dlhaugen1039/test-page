from datetime import datetime as dt
import json

def get_time():
    return dt.now().strftime("%Y-%m-%d %H:%M:%S")

dictionary = {"Last_Updated": get_time()}
with open('result.json', 'w') as fp:
    json.dump(dictionary, fp, indent=4)
print(dictionary)
